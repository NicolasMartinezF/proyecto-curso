import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  },
  {
    path: '/principal',
    name: 'Principal',
    component: () => import(/* webpackChunkName: "Principal" */ '../views/Principal.vue')
  },
  {
    path: '/abono',
    name: 'Abono',
    component: () => import(/* webpackChunkName: "Abono" */ '../views/Abono.vue')

  },
  {
    path: '/retiro',
    name: 'Retirar',
    component: () => import(/* webpackChunkName: "Retirar" */ '../views/Retirar.vue')
  },
  {
    path: '/micuenta',
    name: 'MiCuenta',
    component: () => import(/* webpackChunkName: "Retirar" */ '../views/MiCuenta.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
